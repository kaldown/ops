#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

#define MAXLINE 4096 /*max text line length*/
#define SERV_PORT 3000 /*port*/

int sendall(int sockfd, char *buf, int *len) {
 int total = 0;
 int bytesleft = *len;
 int n;

 while(total < *len){
     n = send(sockfd, buf+total, bytesleft, 0);
     if (n == -1) {break;}
     total += n;
     bytesleft -= n;
 }
 *len = total;

 return n == -1 ? -1 : 0;
}

int main(int argc, char **argv)
{
 int sockfd, n;
 struct sockaddr_in servaddr;
 char sendline[MAXLINE], recvline[MAXLINE];

 //basic check of the arguments
 //additional checks can be inserted
 if (argc !=2) {
  perror("Usage: TCPClient <IP address of the server");
  exit(1);
 }

 //Create a socket for the client
 //If sockfd<0 there was an error in the creation of the socket
 if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) <0) {
  perror("Problem in creating the socket");
  exit(2);
 }

 //Creation of the socket
 memset(&servaddr, 0, sizeof(servaddr));
 servaddr.sin_family = AF_INET;
 servaddr.sin_addr.s_addr= inet_addr(argv[1]);
 servaddr.sin_port =  htons(SERV_PORT); //convert to big-endian order

 //Connection of the client to the socket
 if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr))<0) {
  perror("Problem in connecting to the server");
  exit(3);
 }

 // authentication response
 
 int is_authenticated = 0;
 size_t sendline_s;

 while (!is_authenticated) {
            printf("Client: Enter Data for Server:\n");
            if (fgets(sendline, MAXLINE-1, stdin) != NULL) {
                if (sendline[(strlen(sendline)-1)] == '\n') {

                    sendline[strlen(sendline)-1] = '\0';

                    if ((send(sockfd,sendline, strlen(sendline),0))== -1) {
                            fprintf(stderr, "Failure Sending Message\n");
                            close(sockfd);
                            exit(1);
                    }
                    else {
                            printf("Client:Message being sent: %s\n",sendline);
                            n = recv(sockfd, sendline, sizeof(sendline),0);
                            if ( n <= 0 )
                            {
                                    printf("Either Connection Closed or Error\n");
                                    //Break from the While
                                    break;
                            }

                            sendline[n] = '\0';
                            if (strcmp(sendline, "DONE") == 0) {
                                puts("AUTH PASSED");
                                is_authenticated = 1;
                            }
                            printf("Client:Message Received From Server -  %s\n",sendline);
                    }
                }
                //EOF
            }
 }


  printf(">>>");
  fflush(stdout);
 while (fgets(sendline, MAXLINE, stdin) != NULL) {
     //
     // strlen(BUF)-1 => lead to potentual security issue
     //
  if (sendline[(strlen(sendline)-1)] == '\n') {
      
      sendline[strlen(sendline)-1] = '\0';

      // send won't transfer terminating byte, so 2 choices:
      //send(sockfd, sendline, strlen(sendline)+1, 0);
      send(sockfd, sendline, sizeof sendline, 0);

      printf(">>>");
      fflush(stdout);

     // continue;
  }
  //send(sockfd, sendline, sizeof sendline, 0);
  //printf("\n>>>");
  //fflush(stdout);


  //if (recv(sockfd, recvline, MAXLINE,0) == 0){
  // //error: server terminated prematurely
  // perror("The server terminated prematurely");
  // exit(4);
  //}
  //printf("%s", "String received from the server: ");
  //fputs(recvline, stdout);
 }

 exit(0);
}
