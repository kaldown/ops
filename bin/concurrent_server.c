#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define MAXLINE 4096 /*max text line length*/
#define SERV_PORT 3000 /*port*/
#define LISTENQ 8 /*maximum number of client connections*/

int sendall(int sockfd, char *buf, int *len) {
 int total = 0;
 int bytesleft = *len;
 int n;

 while(total < *len){
     n = send(sockfd, buf+total, bytesleft, 0);
     if (n == -1) {break;}
     total += n;
     bytesleft -= n;
 }
 *len = total;

 return n == -1 ? -1 : 0;
}

int main (int argc, char **argv)
{
 int listenfd, connfd, n;
 pid_t childpid;
 socklen_t clilen;
 char buf[MAXLINE], username[12], password[12];
 struct sockaddr_in cliaddr, servaddr;

 //Create a socket for the soclet
 //If sockfd<0 there was an error in the creation of the socket
 if ((listenfd = socket (AF_INET, SOCK_STREAM, 0)) <0) {
  perror("Problem in creating the socket");
  exit(2);
 }


 //preparation of the socket address
 servaddr.sin_family = AF_INET;
 servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
 servaddr.sin_port = htons(SERV_PORT);

 //bind the socket
 bind (listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

 //listen to the socket by creating a connection queue, then wait for clients
 listen (listenfd, LISTENQ);

 printf("%s\n","Server running...waiting for connections.");

 for ( ; ; ) {

  clilen = sizeof(cliaddr);
  //accept a connection
  connfd = accept (listenfd, (struct sockaddr *) &cliaddr, &clilen);

  printf("%s\n","Received request...");

  if ( (childpid = fork ()) == 0 ) {//if it’s 0, it’s child process

  printf ("%s\n","Child created for dealing with client requests");

  //close listening socket
  close (listenfd);

  // authentication

  int is_authenticated = 0;
  char *accepted = "ACCEPTED_AUTH";
  char name[] = "kaldown";
  char *wrong = "wrong";
  size_t name_s = strlen(name);
  size_t accepted_s = strlen(accepted);
  size_t wrong_s = strlen(wrong);

  while (!is_authenticated) { 

                    if ((n = recv(connfd, buf, MAXLINE-1,0))== -1) {
                            perror("recv");
                            exit(1);
                    }
                    else if (n == 0) {
                            printf("Connection closed\n");
                            //So I can now wait for another client
                            break;
                    }
                    printf("\n%d truely recieved", n);
                    buf[n] = '\0';
                    printf("Server:Msg Received %s\n", buf);
                    if (strcmp(buf, "DONE") == 0) {
                        strcpy(buf, "DONE");
                        if ((send(connfd,buf, strlen(buf),0))== -1)
                        {
                            fprintf(stderr, "Failure Sending Message\n");
                            close(connfd);
                            break;
                        }

                        puts("KONEC");
                        is_authenticated = 1;
                    }
                    else {
                        if ((send(connfd,buf, strlen(buf),0))== -1)
                        {
                            fprintf(stderr, "Failure Sending Message\n");
                            close(connfd);
                            break;
                        }

                    }

                    printf("Server:Msg being sent: %s\nNumber of bytes sent: %d\n", buf, strlen(buf));
}


  while ( (n = recv(connfd, buf, MAXLINE,0)) > 0)  {
   printf("%s","String received from and resent to the client:");
   puts(buf);
  // send(connfd, buf, n, 0);
  }

  if (n < 0)
   printf("%s\n", "Read error");
  exit(0);
 }
 //close socket of the server
 close(connfd);
}
}
