#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>


#define MAXLINE 4096

int main() {
  register int s;
  register int bytes;
  struct sockaddr_in sa;
  char buffer[BUFSIZ+1], sendline[MAXLINE], recvline[MAXLINE];

  if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    return 1;
  }

  bzero(&sa, sizeof sa);

  sa.sin_family = AF_INET;
  sa.sin_port = htons(13);
  sa.sin_addr.s_addr = inet_addr("127.0.0.1");
  if (connect(s, (struct sockaddr *)&sa, sizeof sa) < 0) {
    perror("connect");
    close(s);
    return 2;
  }

  while (fgets(sendline, MAXLINE, stdin) != NULL) {
      send(s, sendline, strlen(sendline), 0);
      if (recv(s, recvline, MAXLINE, 0) == 0) {
                perror("recieve error");
                exit(3);
              }

              fputs(recvline, stdout);
  }

  //while ((bytes = read(s, buffer, BUFSIZ)) > 0)
  //  printf("%d\n", bytes);
  //  write(1, buffer, bytes);

  //close(s);
  //return 0;
  
  exit(0);
}

